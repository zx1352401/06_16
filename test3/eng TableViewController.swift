//
//  eng TableViewController.swift
//  test3
//
//  Created by apple on 2017/4/14.
//  Copyright © 2017年 apple. All rights reserved.
//

import UIKit
import AVFoundation
import  CoreData

class eng_TableViewController: UITableViewController {
    var mycontext : NSManagedObjectContext!
    var english: [EnglishMO] = []
    
    func getContext () -> NSManagedObjectContext{
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    func storeEnglish(){
    let context = getContext()
        var english:EnglishMO!
        english = EnglishMO(context: getContext())
        english.name = "earnings"
        english.cht = "薪水"
        english.speak = "ˋɝnɪŋz"
        if let englishImage = UIImage(named:"earnings.jpg"){
        if let imageData = UIImageJPEGRepresentation(englishImage, 1.0)
        {
            english.picture = NSData(data:imageData )
            
            }
        }
        
        do {
        try context.save()
            print("success")
            
        } catch {
        print(error)
        
        }
        
    }
    func getWord() {
        let request:NSFetchRequest<EnglishMO> = EnglishMO.fetchRequest()
        let context = getContext()
        do {
            english = try context.fetch(request)
            print(english.count)
            for word in english {
                print(word.name!)
                print(word.cht!)
                print(word.speak!)
                
            }
        } catch  {
            print(error)
        }
    }
    func delCoreData() {
        let request:NSFetchRequest<EnglishMO> = EnglishMO.fetchRequest()
        let context = getContext()
        do {
            english = try context.fetch(request)
            print(english.count)
            for word in english {
                print(word.name!)
                print(word.cht!)
                print(word.speak!)
                
            }
        } catch  {
            print(error)
        }
    }

    var englishs : [English] = [
        English(name: "tablespoon",cht:"大湯匙",speak: "tābəlˌspo͞on",picture:"tablespoon.jpg"),
        English(name: "tablet", cht: "平板", speak: "ˈtablət", picture: "tablet.jpg"),
        English(name: "tailor", cht: "裁縫師", speak: "ˈtālər",picture: "tailor.jpg"),
        English(name: "tale", cht: "故事", speak: "tāl", picture: "tale.jpg"),
        English(name: "tangerine", cht: "橘子", speak: "ˌtanjəˈrēn", picture: "tangerine.jpg"),
        English(name: "tanker", cht: "油輪", speak: "ˈtaNGkər", picture: "tanker.jpg"),
        English(name: "technician", cht: "技術人員", speak: "tekˈniSHən", picture: "technician.jpg"),
        English(name: "teenager", cht: "青少年", speak: "ˈtēnˌājər", picture: "teenager.jpg"),
        English(name: "telegram", cht: "電報", speak: "telegram", picture: "telegram.jpg"),
        English(name: "telescope", cht: "望遠鏡", speak: "ˈteləˌskōp", picture: "telescope.jpg"),
        English(name: "television", cht: "電視", speak: "ˈteləˌviZHən", picture: "television.jpg"),
        English(name: "temperature", cht: "溫度", speak: "ˈtemp(ə)rəCHər", picture: "temperature.jpg"),
        English(name: "throat", cht: "喉嚨", speak: "THrōt", picture: "throat.jpg"),
        English(name: "thunderstorm", cht: "大雷雨", speak: "THəndərˌstôrm", picture: "thunderstorm.jpg"),
        English(name: "tissue", cht: "衛生紙", speak: "tiSHo͞o", picture: "tissue.jpg"),
        English(name: "tourist", cht: "觀光客", speak: "ˈto͝orəst", picture: "tourist.jpg"),
        English(name: "trademark", cht: "商標", speak: "ˈtrādˌmärk", picture: "trademark.jpg"),
        English(name: "typhoon", cht: "颱風", speak: "tīˈfo͞on", picture: "typhoon.jpg"),
        English(name: "tuxedo", cht: "燕尾服", speak: "təkˈsēdō", picture: "tuxedo.jpg"),
        English(name: "trumpet", cht: "喇叭", speak: "ˈtrəmpət", picture: "trumpet.jpg"),
        English(name: "tyre", cht: "輪胎", speak: "tī(ə)r", picture: "tyre.jpg"),
        English(name: "trout", cht: "鮭魚", speak: "trout", picture: "trout.jpg"),
        English(name: "trousers", cht: "褲子", speak: "ˈtrouzərz", picture: "trousers.jpg"),
        English(name: "topics", cht: "熱帶", speak: "ˈtäpik", picture: "topics.jpg"),
        English(name: "trolley", cht: "手推車", speak: "ˈträlē", picture: "trolley.jpg"),
        English(name: "tribe", cht: "部落", speak: "trīb", picture: "tribe.jpg"),
        English(name: "translator", cht: "翻譯機", speak: "ˈtransˌlādər", picture: "translator.jpg"),
        English(name: "transfusion", cht: "輸血", speak: "ˌtran(t)sˈfyo͞oZHən", picture: "transfusion.jpg"),
        English(name: "track", cht: "軌道", speak: "trak", picture: "track.jpg"),
        English(name: "township", cht: "鄉", speak: "ˈtounˌSHip", picture: "township.jpg"),
        
    ]
    override var prefersStatusBarHidden: Bool{
        return true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let string = "Hello"
//        let synthesizer = AVSpeechSynthesizer()
//        let voice = AVSpeechSynthesisVoice(language: "en_US")
//        let utterance = AVSpeechUtterance(string: string)
//        utterance.voice = voice
//        synthesizer.speak(utterance)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
      getWord()
        
    }
    

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return englishs.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell{
        let cellid = "eng"
        let cell=tableView.dequeueReusableCell(withIdentifier:cellid , for: indexPath) as! engTableViewCell
        cell.namelabel.text=englishs[indexPath.row].name
        cell.wwlabel.text=englishs[indexPath.row].cht
        cell.aalabel.text=englishs[indexPath.row].speak
        cell.pictures.image=UIImage(named: englishs[indexPath.row].picture)
        return cell
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "englishshow"{
            if let indexPath = tableView.indexPathForSelectedRow{
                let destinationController = segue.destination
                    as! englishViewController
                    destinationController.englishs = englishs[indexPath.row]
                
                
            }
            
            
        }
    }
    //    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //        let optionMenu = UIAlertController(title:nil,message :"what you want to do",preferredStyle:.actionSheet)
    //        let cancelAction = UIAlertAction(title:"cancel",style:.cancel,handler:nil)
    //        optionMenu.addAction(cancelAction)
    //        self.present(optionMenu,animated: true,completion: nil)
    //        let speakActionHandler={(action:UIAlertAction!)->Void in
    //            let string=self.names["eng"]?[indexPath.row]
    //            let synthesizer=AVSpeechSynthesizer()
    //            let voice=AVSpeechSynthesisVoice(language:"en-US")
    //            let utterance = AVSpeechUtterance(string:string!)
    //            utterance.voice=voice
    //            synthesizer.speak(utterance)
    //        }
    //        let englishWords :String = (self.names["eng"]?[indexPath.row])!
    //        let speakAction=UIAlertAction(title:"Speak Word -> \(englishWords)",style:.default,handler:speakActionHandler)
    //        optionMenu.addAction(speakAction)
    //    }
    
    /* override func tableView(_ tableView:UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath){
     
     if editingStyle == .delete{
     
     names["eng"]?.remove(at: indexPath.row)
     names["cht"]?.remove(at: indexPath.row)
     names["speak"]?.remove(at: indexPath.row)
     names["picture"]?.remove(at: indexPath.row)
     
     
     }
     tableView.reloadData()
     print("Total item: \(names.count)")
     for name in names{
     
     print(name)
     }
     }*/
    
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        
        let shareAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title:"share", handler: {(action,  indexPath) -> Void in
            
            let defaultText = "Just checking in at" + self.englishs[indexPath.row].name
            let activityController = UIActivityViewController(activityItems: [defaultText], applicationActivities: nil)
            self.present(activityController, animated: true, completion: nil)
        })
        
        
        let deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title:"DELETE")
        {(action, indexPath) -> Void in
            
            self.englishs.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            
            
            
        }
        let speakAction =  UITableViewRowAction(style: UITableViewRowActionStyle.default, title:"speak")
        {(action, indexPath) -> Void in
            let string=self.englishs[indexPath.row].name
            let synthesizer=AVSpeechSynthesizer()
            let voice=AVSpeechSynthesisVoice(language:"en-US")
            let utterance = AVSpeechUtterance(string:string)
            utterance.voice=voice
            synthesizer.speak(utterance)
            
            
        }
        
        return [deleteAction, shareAction, speakAction]
        
    }
    
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
     
}
